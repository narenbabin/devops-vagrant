# -*- mode: ruby -*-
# vi: set ft=ruby :
$start_script = <<-SCRIPT
docker-compose -f /home/vagrant/docker-compose.yml up -d
SCRIPT

Vagrant.configure("2") do |config|
  config.vm.box = "ubuntu/bionic64"

  # Virtual Box VMs configuration
  config.vm.provider :virtualbox do |vb|
    vb.memory = "4096"
  end

  # Common tools
  config.vm.provision :docker
  config.vm.provision :docker_compose, compose_version: '1.26.2'

  # Virtual Box VMs definition.
  config.vm.define "server-1" do |jenkins|
    jenkins.vm.network "private_network", ip: "192.168.33.60"
    jenkins.vm.hostname = "ubuntu-cd-server-01"
    jenkins.vm.provision :file, source: "../docker/docker-compose.cicd.yml", destination: "/home/vagrant/docker-compose.yml"
    jenkins.vm.provision :shell, inline: "chmod o+rw /var/run/docker.sock"
    jenkins.vm.provision :shell, inline: $start_script
  end

  config.vm.define "server-2" do |gocd|
    gocd.vm.network "private_network", ip: "192.168.33.61"
    gocd.vm.hostname = "ubuntu-cd-server-02"
    gocd.vm.provision :file, source: "../docker/docker-compose.cd.yml", destination: "/home/vagrant/docker-compose.yml"
    gocd.vm.provision :shell, inline: "chmod o+rw /var/run/docker.sock"
    gocd.vm.provision :shell, inline: "export AGENT_AUTO_REGISTER_KEY=#{ENV['AGENT_AUTO_REGISTER_KEY']} && docker-compose -f /home/ubuntu/docker-compose.yml up -d"
  end

  config.vm.define "server-3" do |services|
    services.vm.network "private_network", ip: "192.168.33.62"
    services.vm.hostname = "ubuntu-cd-server-03"
    services.vm.provision :file, source: "../docker/docker-compose.services.yml", destination: "/home/vagrant/docker-compose.yml"
    services.vm.provision :shell, inline: $start_script
  end

  config.vm.define 'server-4' do |selenium|
    selenium.vm.network "private_network", ip: "192.168.33.63"
    selenium.vm.hostname = "ubuntu-cd-server-04"
    selenium.vm.provision :file, source: "../docker/docker-compose.testing.yml", destination: "/home/vagrant/docker-compose.yml"
    selenium.vm.provision :shell, inline: $start_script
  end
end
